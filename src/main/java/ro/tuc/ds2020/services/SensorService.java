package ro.tuc.ds2020.services;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.MonitoredValueRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

import java.util.List;

@Service
public class SensorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);

    private final SensorRepository sensorRepository;
    private final MonitoredValueRepository monitoredValueRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public SensorService(SensorRepository sensorRepository, MonitoredValueRepository monitoredValueRepository, DeviceRepository deviceRepository) {
        this.sensorRepository = sensorRepository;
        this.monitoredValueRepository = monitoredValueRepository;
        this.deviceRepository = deviceRepository;
    }

    public List<Sensor> showAllSensors(){
        return this.sensorRepository.findAll();
    }

    public Sensor findFirstById(Long id){
        return sensorRepository.findFirstById(id);
    }


    public String deleteSensorById(Long id){

        List<Device> devices = deviceRepository.findAll();


        for(Device device : devices){

            if(device.getSensor() != null)
                if(device.getSensor().getId() == id) {
                    device.setSensor(null);

                deviceRepository.save(device);
            }

        }

        sensorRepository.delete(sensorRepository.findFirstById(id));
        LOGGER.info("Sensor with id {} was removed from db",id);
        return "Sensor deleted successfully";
    }


    public Long insert(Sensor sensor){
        sensorRepository.save(sensor);
        LOGGER.info("Sensor with id {} was inserted in db", sensor.getId());
        return sensor.getId();
    }


    public String updateSensor(SensorDTO dto) {

        Sensor aux = sensorRepository.findFirstById(dto.getId());
        aux.setDescription(dto.getDescription());
        aux.setMax_value(dto.getMax_value());

        sensorRepository.save(aux);

        return "Sensor updated";

    }

    public String addSensorMonitoredValue(Long sensorId,Long monitoredValueId){

        Sensor sensor = sensorRepository.findFirstById(sensorId);
        MonitoredValue monitoredValue = monitoredValueRepository.findFirstById(monitoredValueId);

        List<MonitoredValue> monitoredValues = sensor.getMonitoredValueList();

        monitoredValues.add(monitoredValue);
        sensor.setMonitoredValueList(monitoredValues);

        sensorRepository.save(sensor);

        return "Added monitored value " + monitoredValueId + " to sensor " + sensorId;
    }






}
