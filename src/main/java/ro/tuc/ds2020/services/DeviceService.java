package ro.tuc.ds2020.services;

import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceUpdateDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.UserEntity;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class DeviceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);

    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;
    private final SensorRepository sensorRepository;

    @Autowired
    public DeviceService(UserRepository userRepository,
                         DeviceRepository deviceRepository,
                         SensorRepository sensorRepository){
        this.userRepository = userRepository;
        this.deviceRepository = deviceRepository;
        this.sensorRepository = sensorRepository;
    }

    public List<Device> showAllDevices(){
        return this.deviceRepository.findAll();
    }

    public Device findFirstById(Long id){
        return deviceRepository.findFirstById(id);
    }

    public Long insert(DeviceUpdateDTO deviceUpdateDTO){
        Device device = DeviceBuilder.toDeviceFromDeviceUpdateDTO(deviceUpdateDTO);
        device = deviceRepository.save(device);
        LOGGER.info("Device with id {} was inserted in db", device.getId());
        return device.getId();
    }

    public String deleteDeviceById(Long id){

        List<UserEntity> userEntityList = userRepository.findAll();
        for(UserEntity user : userEntityList){

            if(user.getDeviceList()!=null) {
                List<Device> deviceList;
                deviceList = user.getDeviceList();

                deviceList.removeIf(device -> Objects.equals(device.getId(), id));

                user.setDeviceList(deviceList);
                userRepository.save(user);
            }
        }

        deviceRepository.delete(deviceRepository.findFirstById(id));
        LOGGER.info("Device with id {} was removed from db",id);
        return "Device deleted";
    }

    public String updateDevice(DeviceUpdateDTO dto){

        Device aux = deviceRepository.findFirstById(dto.getId());

        aux.setAverageEnergyConsumption(dto.getAverageEnergyConsumption());
        aux.setDescription(dto.getDescription());
        aux.setLocation(dto.getLocation());
        aux.setMaxEnergyConsumption(dto.getMaxEnergyConsumption());

        deviceRepository.save(aux);

        return "Device updated";
    }

    public String addDeviceSensor(Long deviceId, Long sensorId){

        Device device = deviceRepository.findFirstById(deviceId);
        Sensor sensor = sensorRepository.findFirstById(sensorId);

        device.setSensor(sensor);
        deviceRepository.save(device);

        return "Added sensor " + sensorId + " to device " + deviceId;
    }

    public String removeDeviceSensor(Long deviceId, Long sensorId){

        Device device = deviceRepository.findFirstById(deviceId);

        device.setSensor(null);
        deviceRepository.save(device);

        return "Removed sensor " + sensorId + " from device " + deviceId;

    }



}
