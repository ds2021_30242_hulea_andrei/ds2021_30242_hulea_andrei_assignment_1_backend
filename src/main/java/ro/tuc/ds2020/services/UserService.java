package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserUpdateDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.UserEntity;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public UserService(UserRepository userRepository,
                       DeviceRepository deviceRepository) {
        this.userRepository = userRepository;
        this.deviceRepository = deviceRepository;
    }

    public List<UserDTO> findUsers() {
        return UserBuilder.toUserDTOList(userRepository.findAll());
    }

    public UserDTO findUserDetailsDTOById(Long id) {
        Optional<UserEntity> prosumerOptional = userRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.info("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(UserEntity.class.getSimpleName() + " with id: " + id);
        }
        return UserBuilder.toUserDTO(prosumerOptional.get());
    }


    public Long insert(UserUpdateDTO userDTO) {
        UserEntity userEntity = UserBuilder.toUserFromUserUpdateDTO(userDTO);
        userEntity = userRepository.save(userEntity);
        LOGGER.info("User with id {} was inserted in db", userEntity.getId());
        return userEntity.getId();
    }


    public UserEntity findUserById(Long id) {

        return userRepository.findFirstById(id);
    }

    public String deleteUserById(Long id) {
        userRepository.delete(userRepository.findFirstById(id));
        LOGGER.debug("User with id {} was removed from db", id);
        return "User deleted";
    }


    public String updateUserByUsername(UserDTO dto) {

        if (userRepository.findFirstByUsername(dto.getUsername()) == null) {
            LOGGER.info("User not found");
            return "User not found";
        } else {
            UserEntity aux = userRepository.findFirstByUsername(dto.getUsername());

            aux.setName(dto.getName());
            aux.setAge(dto.getAge());
            aux.setUsername(dto.getUsername());
            aux.setPassword(dto.getPassword());

            userRepository.save(aux);

            LOGGER.info("User data updated");
            return "User data updated";
        }
    }

    public String updateUserById(UserUpdateDTO dto) {

        if (userRepository.findFirstById(dto.getId()) == null) {
            LOGGER.info("User not found");
            return "User not found";
        } else {
            UserEntity aux = userRepository.findFirstById(dto.getId());


            userRepository.save(UserEntity.builder()
                    .id(dto.getId())
                    .name(dto.getName())
                    .age(dto.getAge())
                    .address(dto.getAddress())
                    .username(dto.getUsername())
                    .password(dto.getPassword())
                    .role(dto.getRole()).build());

            LOGGER.info("User data updated");
            return "User data updated";
        }

    }

    public String addUserDevice(Long userId, Long deviceId) {

        Device device = deviceRepository.findFirstById(deviceId);
        UserEntity user = userRepository.findFirstById(userId);

        List<Device> userDeviceList = user.getDeviceList();

        userDeviceList.add(device);
        user.setDeviceList(userDeviceList);

        userRepository.save(user);

        return "Added device " + deviceId + " to user " + userId;

    }

    public String removeUserDevice(Long userId, Long deviceId) {

        Device device = deviceRepository.findFirstById(deviceId);
        UserEntity user = userRepository.findFirstById(userId);

        List<Device> userDeviceList = user.getDeviceList();

        userDeviceList.remove(device);
        user.setDeviceList(userDeviceList);

        userRepository.save(user);

        return "Removed device " + deviceId + " from user " + userId;

    }

    public List<Device> getUserDevices(Long userId) {

        return userRepository.findFirstById(userId).getDeviceList();

    }

    public List<Sensor> getUserDevicesSensors(Long userId) {

        List<Device> deviceList = getUserDevices(userId);

        List<Sensor> sensorList = new ArrayList<>();

        for (Device i : deviceList)
            if (i.getSensor() != null)
                sensorList.add(i.getSensor());


        return sensorList;
    }

    public List<MonitoredValue> getUserDevicesSensorsMonitoredValues(Long userId) {

        List<Sensor> sensorList = getUserDevicesSensors(userId);

        List<MonitoredValue> monitoredValues = new ArrayList<>();

        for (Sensor i : sensorList)
            if (i.getMonitoredValueList() != null)
                monitoredValues.addAll(i.getMonitoredValueList());

        return monitoredValues;

    }

    public Double totalConsumption(Long userId) {

        List<MonitoredValue> monitoredValues = getUserDevicesSensorsMonitoredValues(userId);

        double sum = 0;

        for (MonitoredValue i : monitoredValues)
            sum += i.getValue();

        return sum;

    }

}
