package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.repositories.MonitoredValueRepository;

import java.util.List;

@Service
public class MonitoredValueService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitoredValueService.class);

    private final MonitoredValueRepository monitoredValueRepository;

    @Autowired
    public MonitoredValueService(MonitoredValueRepository monitoredValueRepository) {
        this.monitoredValueRepository = monitoredValueRepository;
    }

    public List<MonitoredValue> showAllMonitoredVlaues(){
        return this.monitoredValueRepository.findAll();
    }

    public MonitoredValue findFirstById(Long id){
       return monitoredValueRepository.findFirstById(id);
    }

    public Long insert(MonitoredValue monitoredValue){
        monitoredValueRepository.save(monitoredValue);
        LOGGER.debug("Monitored value with id {} was inserted in db", monitoredValue.getId());
        return monitoredValue.getId();
    }

    public String deleteMonitoredValueById(Long id){
        monitoredValueRepository.delete(monitoredValueRepository.findFirstById(id));
        LOGGER.debug("Monitored value with id {} was removed from db",id);
        return "Monitored value deleted successfully";
    }

    public String updateMonitoredValue(MonitoredValueDTO dto){

        MonitoredValue aux = monitoredValueRepository.findFirstById(dto.getId());

        aux.setTimestamp(dto.getTimestamp());
        aux.setValue(dto.getValue());

        monitoredValueRepository.save(aux);

        return "Monitored value updated";

    }
}
