package ro.tuc.ds2020.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QueueDTO {

   private Long id;
   private double value;
   private Long timestamp;

}
