package ro.tuc.ds2020.dtos;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ro.tuc.ds2020.entities.MonitoredValue;

import java.util.List;

@Getter
@Setter
@Builder
public class SensorDTO {

    private Long id;
    private String description;
    private double max_value;
    private List<MonitoredValueDTO> monitoredValueList;
}
