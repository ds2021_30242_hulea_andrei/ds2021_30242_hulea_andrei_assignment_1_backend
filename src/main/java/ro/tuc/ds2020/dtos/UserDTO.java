package ro.tuc.ds2020.dtos;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ro.tuc.ds2020.entities.UserEntity;

import java.util.List;

@Getter
@Setter
@Builder
public class UserDTO {

    private Long id;
    private String name;
    private int age;

    private String address;
    private String username;
    private String password;
    private UserEntity.Role role;

    private List<DeviceDTO> deviceList;

}
