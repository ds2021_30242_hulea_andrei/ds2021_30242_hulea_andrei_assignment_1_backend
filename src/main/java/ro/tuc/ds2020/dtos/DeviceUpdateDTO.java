package ro.tuc.ds2020.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DeviceUpdateDTO {

    private Long id;
    private String description;
    private String location;
    private double maxEnergyConsumption;
    private double averageEnergyConsumption;
}
