package ro.tuc.ds2020.dtos.builders;


import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceUpdateDTO;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.entities.Device;

import java.util.List;
import java.util.stream.Collectors;

public class DeviceBuilder {

    public static DeviceDTO toDeviceDTO(Device device){

        if(device.getSensor() == null)
        return DeviceDTO
                .builder()
                .id(device.getId())
                .description(device.getDescription())
                .location(device.getLocation())
                .averageEnergyConsumption(device.getAverageEnergyConsumption())
                .maxEnergyConsumption(device.getMaxEnergyConsumption())
                .build();
        else
            return DeviceDTO
                    .builder()
                    .id(device.getId())
                    .description(device.getDescription())
                    .location(device.getLocation())
                    .averageEnergyConsumption(device.getAverageEnergyConsumption())
                    .maxEnergyConsumption(device.getMaxEnergyConsumption())
                    .sensor(SensorBuilder.toSensorDTO(device.getSensor()))
                    .build();
    }

    public static DeviceUpdateDTO toDeviceUpdateDTO(Device device){
        return DeviceUpdateDTO.builder()
                .id(device.getId())
                .description(device.getDescription())
                .location(device.getLocation())
                .averageEnergyConsumption(device.getAverageEnergyConsumption())
                .maxEnergyConsumption(device.getMaxEnergyConsumption())
                .build();
    }

    public static Device toDevice(DeviceDTO deviceDTO){
        return Device.builder()
                .id(deviceDTO.getId())
                .description(deviceDTO.getDescription())
                .location(deviceDTO.getLocation())
                .averageEnergyConsumption(deviceDTO.getAverageEnergyConsumption())
                .maxEnergyConsumption(deviceDTO.getMaxEnergyConsumption())
                .sensor(SensorBuilder.toSensor(deviceDTO.getSensor()))
                .build();
    }

    public static Device toDeviceFromDeviceUpdateDTO(DeviceUpdateDTO deviceUpdateDTO){
        return Device.builder()
                .id(deviceUpdateDTO.getId())
                .description(deviceUpdateDTO.getDescription())
                .location(deviceUpdateDTO.getLocation())
                .averageEnergyConsumption(deviceUpdateDTO.getAverageEnergyConsumption())
                .maxEnergyConsumption(deviceUpdateDTO.getMaxEnergyConsumption())
                .build();
    }

}
