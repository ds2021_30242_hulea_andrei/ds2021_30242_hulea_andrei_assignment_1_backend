package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.UserUpdateDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserBuilder {

    private UserBuilder() {
    }

    public static List<UserDTO> toUserDTOList(List<UserEntity> userEntityList){

        List<UserDTO> aux = userEntityList.stream().map(UserBuilder::toUserDTO).collect(Collectors.toList());
        return aux;

    }

    public static UserDTO toUserDTO(UserEntity userEntity) {

        List<DeviceDTO> devicesDTO = new ArrayList<>();

        if(userEntity.getDeviceList() != null)
         devicesDTO = userEntity.getDeviceList()
                        .stream()
                        .map(DeviceBuilder::toDeviceDTO)
                        .collect(Collectors.toList());


        return UserDTO.builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .age(userEntity.getAge())
                .address(userEntity.getAddress())
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .role(userEntity.getRole())
                .deviceList(devicesDTO)
                .build();

    }


    public static UserUpdateDTO toUserUpdateDTO(UserEntity userEntity) {

        return UserUpdateDTO.builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .age(userEntity.getAge())
                .address(userEntity.getAddress())
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .role(userEntity.getRole())
                .build();
    }


    public static UserEntity toUser(UserDTO dto) {

        List<Device> devices = new ArrayList<>();

        if(dto.getDeviceList()!=null)
        {    devices =
                    dto.getDeviceList()
                            .stream()
                            .map(DeviceBuilder::toDevice)
                            .collect(Collectors.toList());
        }


        return ro.tuc.ds2020.entities.UserEntity.builder()
                .id(dto.getId())
                .name(dto.getName())
                .age(dto.getAge())
                .address(dto.getAddress())
                .username(dto.getUsername())
                .password(dto.getPassword())
                .role(dto.getRole())
                .deviceList(devices)
                .build();
    }

    public static UserEntity toUserFromUserUpdateDTO(UserUpdateDTO dto) {

        return ro.tuc.ds2020.entities.UserEntity.builder()
                .id(dto.getId())
                .name(dto.getName())
                .age(dto.getAge())
                .address(dto.getAddress())
                .username(dto.getUsername())
                .password(dto.getPassword())
                .role(dto.getRole())
                .build();
    }


}
