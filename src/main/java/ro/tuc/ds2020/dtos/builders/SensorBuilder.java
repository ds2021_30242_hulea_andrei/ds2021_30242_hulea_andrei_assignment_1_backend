package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SensorBuilder {

    public static SensorDTO toSensorDTO(Sensor sensor){

        List<MonitoredValueDTO> monitoredValueDTOS = new ArrayList<>();

        if(sensor.getMonitoredValueList() != null)
            monitoredValueDTOS =
                sensor.getMonitoredValueList()
                        .stream()
                        .map(MonitoredValueBuilder::toMonitoredValueDTO)
                        .collect(Collectors.toList());



        return SensorDTO.builder()
                .id(sensor.getId())
                .description(sensor.getDescription())
                .max_value(sensor.getMax_value())
                .monitoredValueList(monitoredValueDTOS)
                .build();
    }

    public static Sensor toSensor(SensorDTO dto){

        List<MonitoredValue> monitoredValues = new ArrayList<>();

        if(dto.getMonitoredValueList() != null)
        monitoredValues =
                dto.getMonitoredValueList()
                        .stream()
                        .map(MonitoredValueBuilder::toMonitoredValue)
                        .collect(Collectors.toList());


        return Sensor.builder()
                .id(dto.getId())
                .description(dto.getDescription())
                .max_value(dto.getMax_value())
                .monitoredValueList(monitoredValues)
                .build();
    }
}
