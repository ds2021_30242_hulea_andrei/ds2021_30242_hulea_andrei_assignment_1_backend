package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.entities.MonitoredValue;


public class MonitoredValueBuilder {

    public static MonitoredValueDTO toMonitoredValueDTO(MonitoredValue monitoredValue){
        return MonitoredValueDTO.builder()
                .id(monitoredValue.getId())
                .timestamp(monitoredValue.getTimestamp())
                .value(monitoredValue.getValue())
                .build();
    }

    public static MonitoredValue toMonitoredValue(MonitoredValueDTO dto){
        return MonitoredValue.builder()
                .id(dto.getId())
                .timestamp(dto.getTimestamp())
                .value(dto.getValue())
                .build();
    }
}
