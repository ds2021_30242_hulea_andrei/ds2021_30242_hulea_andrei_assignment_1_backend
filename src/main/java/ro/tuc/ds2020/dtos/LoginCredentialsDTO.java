package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginCredentialsDTO {

    private String username;
    private String password;
}
