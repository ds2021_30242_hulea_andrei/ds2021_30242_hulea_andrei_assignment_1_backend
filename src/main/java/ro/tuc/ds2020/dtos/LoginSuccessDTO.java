package ro.tuc.ds2020.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ro.tuc.ds2020.entities.UserEntity;


@Setter
@Getter
@Builder
public class LoginSuccessDTO {
    private UserEntity.Role role;
    private String id;
}
