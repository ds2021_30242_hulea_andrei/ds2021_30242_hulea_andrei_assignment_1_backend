package ro.tuc.ds2020.dtos;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import ro.tuc.ds2020.entities.Sensor;

@Getter
@Setter
@Builder
public class DeviceDTO{

    private Long id;
    private String description;
    private String location;
    private double maxEnergyConsumption;
    private double averageEnergyConsumption;
    private SensorDTO sensor;
}
