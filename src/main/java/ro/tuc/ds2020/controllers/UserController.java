package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserUpdateDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.services.UserService;

import javax.validation.Valid;
import java.util.List;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<UserDTO> dtos = userService.findUsers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") Long userId) {
        UserDTO dto = userService.findUserDetailsDTOById(userId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertUser(@Valid @RequestBody UserUpdateDTO user) {
        return new ResponseEntity<>(userService.insert(user), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long userId){
        return new ResponseEntity<>( userService.deleteUserById(userId), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateUser(@Valid @RequestBody UserUpdateDTO dto){
        return new ResponseEntity<>( userService.updateUserById(dto), HttpStatus.OK);
    }


    @PutMapping("/{userId}/{deviceId}")
    public ResponseEntity<String> addUserDevice(@PathVariable Long userId, @PathVariable Long deviceId){

        return new ResponseEntity<>( userService.addUserDevice(userId,deviceId), HttpStatus.OK);
    }

    @PutMapping("/remove/{userId}/{deviceId}")
    public ResponseEntity<String> removeUserDevice(@PathVariable Long userId, @PathVariable Long deviceId){

        return new ResponseEntity<>(userService.removeUserDevice(userId,deviceId),HttpStatus.OK);
    }

    @GetMapping("/devices/{id}")
    public ResponseEntity<List<Device>> getUserDevices(@PathVariable("id") Long id){
        return new ResponseEntity<>(userService.getUserDevices(id),HttpStatus.OK);
    }


    @GetMapping("/devices/sensors/{id}")
    public ResponseEntity<List<Sensor>> getUserDevicesSensors(@PathVariable("id") Long id){
        return new ResponseEntity<>(userService.getUserDevicesSensors(id),HttpStatus.OK);
    }

    @GetMapping("/devices/sensors/monitored/{id}")
    public ResponseEntity<List<MonitoredValue>> getUserDevicesSensorsMonitoredValues(@PathVariable("id") Long id){
        return new ResponseEntity<>(userService.getUserDevicesSensorsMonitoredValues(id),HttpStatus.OK);
    }

    @GetMapping("/sum/{id}")
    public ResponseEntity<Double> totalConsumption(@PathVariable("id") Long userId){
        return new ResponseEntity<>(userService.totalConsumption(userId),HttpStatus.OK);
    }



}
