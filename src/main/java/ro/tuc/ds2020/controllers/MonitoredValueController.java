package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.services.MonitoredValueService;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/monitored")
public class MonitoredValueController {

    private final MonitoredValueService monitoredValueService;

    @Autowired
    public MonitoredValueController(MonitoredValueService monitoredValueService) {
        this.monitoredValueService = monitoredValueService;
    }

    @GetMapping()
    public ResponseEntity<List<MonitoredValue>> findAllMonitoredValues(){
        return new ResponseEntity<>(monitoredValueService.showAllMonitoredVlaues(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MonitoredValue> getMonitoredValue(@PathVariable("id") Long id){
        MonitoredValue aux = monitoredValueService.findFirstById(id);
        return new ResponseEntity<>(aux,HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertMonitoredValue(@RequestBody @Valid MonitoredValue monitoredValue){
        Long id = monitoredValueService.insert(monitoredValue);

//        MonitoredValue aux = MonitoredValue.builder()
//                .value(123.12)
//                .timestamp(LocalDateTime.now())
//                .build();

       // monitoredValueService.insert(aux);

        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteMonitoredValue(@PathVariable("id") Long id){
        monitoredValueService.deleteMonitoredValueById(id);
        return new ResponseEntity<>("Device deleted successfully",HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateMonitoredValue(@RequestBody MonitoredValueDTO dto){
        monitoredValueService.updateMonitoredValue(dto);
        return new ResponseEntity<>("Monitored value updated",HttpStatus.OK);
    }
}
