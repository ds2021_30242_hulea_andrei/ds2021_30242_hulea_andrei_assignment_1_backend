package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.services.SensorService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/sensor")
public class SensorController {

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @GetMapping()
    public ResponseEntity<List<Sensor>> findAllSensors(){

        List<Sensor> sensorList = sensorService.showAllSensors();
        return new ResponseEntity<>(sensorList,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Sensor> getSensor(@PathVariable("id") Long id){

        Sensor aux = sensorService.findFirstById(id);
        return new ResponseEntity<>(aux,HttpStatus.OK);

    }

    @PostMapping()
    public ResponseEntity<Long> insertSensor(@RequestBody @Valid Sensor sensor){
        Long id = sensorService.insert(sensor);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSensor(@PathVariable("id") Long id){

        return new ResponseEntity<>(sensorService.deleteSensorById(id),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateSensor(@RequestBody SensorDTO dto){

        return new ResponseEntity<>(sensorService.updateSensor(dto),HttpStatus.OK);
    }

    @PutMapping("/{sensorId}/{monitoredValueId}")
    public ResponseEntity<String> addSensorMonitoredValue(@PathVariable Long sensorId,
                                                          @PathVariable Long monitoredValueId){
        return new ResponseEntity<>(sensorService
                .addSensorMonitoredValue(sensorId,monitoredValueId),
                HttpStatus.OK);
    }


}
