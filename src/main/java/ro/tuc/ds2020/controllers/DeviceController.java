package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceUpdateDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.services.DeviceService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/device")
public class DeviceController {

    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }


    @GetMapping()
    public ResponseEntity<List<Device>> findAllDevices() {
        return new ResponseEntity<>(deviceService.showAllDevices(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Device> getDevice(@PathVariable("id") Long id) {

        return new ResponseEntity<>(deviceService.findFirstById(id), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertDevice(@RequestBody @Valid DeviceUpdateDTO device) {

        return new ResponseEntity<>(deviceService.insert(device), HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteDevice(@PathVariable("id") Long id) {
        return new ResponseEntity<>(deviceService.deleteDeviceById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateDevice(@RequestBody DeviceUpdateDTO dto) {
        return new ResponseEntity<>(deviceService.updateDevice(dto), HttpStatus.OK);
    }

    @PutMapping("/{deviceId}/{sensorId}")
    public ResponseEntity<String> addDeviceSensor(@PathVariable Long deviceId,@PathVariable Long sensorId){
        return new ResponseEntity<>(deviceService.addDeviceSensor(deviceId,sensorId), HttpStatus.OK);
    }

    @PutMapping("/remove/{deviceId}/{sensorId}")
    public ResponseEntity<String> removeDeviceSensor(@PathVariable Long deviceId,@PathVariable Long sensorId){
        return new ResponseEntity<>(deviceService.removeDeviceSensor(deviceId,sensorId), HttpStatus.OK);
    }
}
