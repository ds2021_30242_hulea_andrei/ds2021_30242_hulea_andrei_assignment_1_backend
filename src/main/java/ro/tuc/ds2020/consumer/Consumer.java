package ro.tuc.ds2020.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.QueueDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.services.MonitoredValueService;
import ro.tuc.ds2020.services.SensorService;
import ro.tuc.ds2020.services.UserService;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;


@Component
public class Consumer {

    private final SensorService sensorService;
    private final MonitoredValueService monitoredValueService;
    private final Channel channel;
    private final SimpMessagingTemplate template;
    private final UserService userService;

    @SneakyThrows
    @Autowired
    public Consumer(SensorService sensorService,
                    MonitoredValueService monitoredValueService, SimpMessagingTemplate template, UserService userService) {

        String uri = System.getenv("CLOUDAMQP_URL");
        if (uri == null)
            uri = "amqps://zamiwhek:8kWcKvPObqQyH3cMZPzHcFf1w7ZS6sZl@roedeer.rmq.cloudamqp.com/zamiwhek";

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);
        factory.setConnectionTimeout(30000);

        Connection connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare("monitored_values_queue", false, false, false, null);

        this.sensorService = sensorService;
        this.monitoredValueService = monitoredValueService;
        this.template = template;
        this.userService = userService;
    }

    @SneakyThrows
    @Scheduled(fixedDelay = 1000)
    public void handleMessage() {

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);

            ObjectMapper mapper = new ObjectMapper();
            QueueDTO queueDTO = mapper.readValue(message, QueueDTO.class);


            MonitoredValue newMonitoredValue = MonitoredValue.builder()
                    .value(queueDTO.getValue())
                    .timestamp(Instant.ofEpochSecond(queueDTO.getTimestamp()).atZone(ZoneId.of("Europe/Bucharest")).toLocalDateTime()).build();

            System.out.println("New monitored value: " + newMonitoredValue);
            monitoredValueService.insert(newMonitoredValue);

//            MonitoredValue previousMonitoredValue = monitoredValueService.showAllMonitoredVlaues()
//                    .get(monitoredValueService.showAllMonitoredVlaues().size() - 2);
            MonitoredValue previousMonitoredValue = sensorService.findFirstById(queueDTO.getId())
                    .getMonitoredValueList()
                    .get(sensorService.findFirstById(queueDTO.getId())
                            .getMonitoredValueList().size() - 2);

            sensorService.addSensorMonitoredValue(queueDTO.getId(), newMonitoredValue.getId());

            System.out.println("Previous monitored value: " + previousMonitoredValue);


            double peak = (newMonitoredValue.getValue() - previousMonitoredValue.getValue())/
                    (queueDTO.getTimestamp()- previousMonitoredValue.getTimestamp().atZone(ZoneId.of("Europe/Bucharest")).toEpochSecond());

            System.out.println("PEAK for sensor " + queueDTO.getId() + " : " + peak);

            double maxValue = sensorService.findFirstById(queueDTO.getId()).getMax_value();

            Long userId = 0L;
            List<UserDTO> userEntityList = userService.findUsers();
            for(UserDTO user : userEntityList){
                for(DeviceDTO device : user.getDeviceList()){
                    if (device.getSensor() != null && Objects.equals(device.getSensor().getId(), queueDTO.getId())) {
                        userId = user.getId();
                        break;
                    }
                }
            }

            if(peak > maxValue) {
                template.convertAndSend("/topic/socket/notif/" + userId,
                        "Sensor with id " + queueDTO.getId() + " with maximum value = " + maxValue + " has reached a new peak = " + peak);
            }

            System.out.println(" [x] Received '" + message + "'\n");
        };


        channel.basicConsume("monitored_values_queue", true, deliverCallback, consumerTag -> {});

    }

}
