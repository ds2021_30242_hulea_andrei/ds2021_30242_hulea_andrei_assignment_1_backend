package ro.tuc.ds2020.entities;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Device implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String description;
    private String location;
    private double maxEnergyConsumption;
    private double averageEnergyConsumption;


    @OneToOne
    private Sensor sensor;
}
