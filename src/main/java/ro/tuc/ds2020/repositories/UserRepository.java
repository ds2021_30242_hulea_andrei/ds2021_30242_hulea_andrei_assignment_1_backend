package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    /**
     * Example: JPA generate Query by Field
     */
    List<UserEntity> findByName(String name);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM UserEntity p " +
            "WHERE p.name = :name " +
            "AND p.age >= 60  ")
    Optional<UserEntity> findSeniorsByName(@Param("name") String name);

    //@Query(value = "select user_entity_id from user_entity_device_list where device_list_id")


    UserEntity findFirstById(Long id);

    UserEntity findFirstByUsername(String username);


}
