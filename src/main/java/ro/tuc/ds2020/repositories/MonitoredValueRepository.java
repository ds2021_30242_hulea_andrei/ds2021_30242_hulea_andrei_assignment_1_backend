package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.MonitoredValue;

public interface MonitoredValueRepository extends JpaRepository<MonitoredValue,Long> {

    MonitoredValue findFirstById(Long id);

}
