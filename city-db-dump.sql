PGDMP     /    "            
    y            city-db    13.4    13.4      �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16403    city-db    DATABASE     h   CREATE DATABASE "city-db" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Romanian_Romania.1250';
    DROP DATABASE "city-db";
                postgres    false            �            1259    17352    device    TABLE     
  CREATE TABLE public.device (
    id bigint NOT NULL,
    average_energy_consumption double precision NOT NULL,
    description character varying(255),
    location character varying(255),
    max_energy_consumption double precision NOT NULL,
    sensor_id bigint
);
    DROP TABLE public.device;
       public         heap    postgres    false            �            1259    17350    hibernate_sequence    SEQUENCE     {   CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public          postgres    false            �            1259    17222    hibernate_sequences    TABLE     t   CREATE TABLE public.hibernate_sequences (
    sequence_name character varying(255) NOT NULL,
    next_val bigint
);
 '   DROP TABLE public.hibernate_sequences;
       public         heap    postgres    false            �            1259    17360    monitored_value    TABLE     �   CREATE TABLE public.monitored_value (
    id bigint NOT NULL,
    "timestamp" timestamp without time zone,
    value double precision NOT NULL
);
 #   DROP TABLE public.monitored_value;
       public         heap    postgres    false            �            1259    17365    sensor    TABLE     �   CREATE TABLE public.sensor (
    id bigint NOT NULL,
    description character varying(255),
    max_value double precision NOT NULL
);
    DROP TABLE public.sensor;
       public         heap    postgres    false            �            1259    17370    sensor_monitored_value_list    TABLE     �   CREATE TABLE public.sensor_monitored_value_list (
    sensor_id bigint NOT NULL,
    monitored_value_list_id bigint NOT NULL
);
 /   DROP TABLE public.sensor_monitored_value_list;
       public         heap    postgres    false            �            1259    17435    user_entity    TABLE     $  CREATE TABLE public.user_entity (
    id bigint NOT NULL,
    address character varying(255) NOT NULL,
    age integer NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    role integer NOT NULL,
    username character varying(255) NOT NULL
);
    DROP TABLE public.user_entity;
       public         heap    postgres    false            �            1259    17443    user_entity_device_list    TABLE     x   CREATE TABLE public.user_entity_device_list (
    user_entity_id bigint NOT NULL,
    device_list_id bigint NOT NULL
);
 +   DROP TABLE public.user_entity_device_list;
       public         heap    postgres    false            �          0    17352    device 
   TABLE DATA           z   COPY public.device (id, average_energy_consumption, description, location, max_energy_consumption, sensor_id) FROM stdin;
    public          postgres    false    202   j'       �          0    17222    hibernate_sequences 
   TABLE DATA           F   COPY public.hibernate_sequences (sequence_name, next_val) FROM stdin;
    public          postgres    false    200   �'       �          0    17360    monitored_value 
   TABLE DATA           A   COPY public.monitored_value (id, "timestamp", value) FROM stdin;
    public          postgres    false    203   (       �          0    17365    sensor 
   TABLE DATA           <   COPY public.sensor (id, description, max_value) FROM stdin;
    public          postgres    false    204   ~(       �          0    17370    sensor_monitored_value_list 
   TABLE DATA           Y   COPY public.sensor_monitored_value_list (sensor_id, monitored_value_list_id) FROM stdin;
    public          postgres    false    205   �(       �          0    17435    user_entity 
   TABLE DATA           W   COPY public.user_entity (id, address, age, name, password, role, username) FROM stdin;
    public          postgres    false    206   0)       �          0    17443    user_entity_device_list 
   TABLE DATA           Q   COPY public.user_entity_device_list (user_entity_id, device_list_id) FROM stdin;
    public          postgres    false    207   x)       �           0    0    hibernate_sequence    SEQUENCE SET     A   SELECT pg_catalog.setval('public.hibernate_sequence', 47, true);
          public          postgres    false    201            ?           2606    17359    device device_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.device DROP CONSTRAINT device_pkey;
       public            postgres    false    202            =           2606    17226 ,   hibernate_sequences hibernate_sequences_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.hibernate_sequences
    ADD CONSTRAINT hibernate_sequences_pkey PRIMARY KEY (sequence_name);
 V   ALTER TABLE ONLY public.hibernate_sequences DROP CONSTRAINT hibernate_sequences_pkey;
       public            postgres    false    200            A           2606    17364 $   monitored_value monitored_value_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.monitored_value
    ADD CONSTRAINT monitored_value_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.monitored_value DROP CONSTRAINT monitored_value_pkey;
       public            postgres    false    203            C           2606    17369    sensor sensor_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.sensor DROP CONSTRAINT sensor_pkey;
       public            postgres    false    204            I           2606    17447 4   user_entity_device_list uk_2bwnnkn7w1qsa3nkvf4bt3yu9 
   CONSTRAINT     y   ALTER TABLE ONLY public.user_entity_device_list
    ADD CONSTRAINT uk_2bwnnkn7w1qsa3nkvf4bt3yu9 UNIQUE (device_list_id);
 ^   ALTER TABLE ONLY public.user_entity_device_list DROP CONSTRAINT uk_2bwnnkn7w1qsa3nkvf4bt3yu9;
       public            postgres    false    207            E           2606    17388 8   sensor_monitored_value_list uk_l1eawac5agalqy8d3u492l0sx 
   CONSTRAINT     �   ALTER TABLE ONLY public.sensor_monitored_value_list
    ADD CONSTRAINT uk_l1eawac5agalqy8d3u492l0sx UNIQUE (monitored_value_list_id);
 b   ALTER TABLE ONLY public.sensor_monitored_value_list DROP CONSTRAINT uk_l1eawac5agalqy8d3u492l0sx;
       public            postgres    false    205            G           2606    17442    user_entity user_entity_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.user_entity DROP CONSTRAINT user_entity_pkey;
       public            postgres    false    206            L           2606    17401 7   sensor_monitored_value_list fk3it9nujd3kk2f0lj8ver4gqi5    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensor_monitored_value_list
    ADD CONSTRAINT fk3it9nujd3kk2f0lj8ver4gqi5 FOREIGN KEY (sensor_id) REFERENCES public.sensor(id);
 a   ALTER TABLE ONLY public.sensor_monitored_value_list DROP CONSTRAINT fk3it9nujd3kk2f0lj8ver4gqi5;
       public          postgres    false    204    2883    205            K           2606    17396 7   sensor_monitored_value_list fk60jwwcity2yd6dumqcfmf13xf    FK CONSTRAINT     �   ALTER TABLE ONLY public.sensor_monitored_value_list
    ADD CONSTRAINT fk60jwwcity2yd6dumqcfmf13xf FOREIGN KEY (monitored_value_list_id) REFERENCES public.monitored_value(id);
 a   ALTER TABLE ONLY public.sensor_monitored_value_list DROP CONSTRAINT fk60jwwcity2yd6dumqcfmf13xf;
       public          postgres    false    205    2881    203            J           2606    17391 "   device fkiyue38c0yw9lbg5856a6e846o    FK CONSTRAINT     �   ALTER TABLE ONLY public.device
    ADD CONSTRAINT fkiyue38c0yw9lbg5856a6e846o FOREIGN KEY (sensor_id) REFERENCES public.sensor(id);
 L   ALTER TABLE ONLY public.device DROP CONSTRAINT fkiyue38c0yw9lbg5856a6e846o;
       public          postgres    false    204    202    2883            N           2606    17453 3   user_entity_device_list fkli3ghs7anbwau3obf8mfnnmw2    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_entity_device_list
    ADD CONSTRAINT fkli3ghs7anbwau3obf8mfnnmw2 FOREIGN KEY (user_entity_id) REFERENCES public.user_entity(id);
 ]   ALTER TABLE ONLY public.user_entity_device_list DROP CONSTRAINT fkli3ghs7anbwau3obf8mfnnmw2;
       public          postgres    false    207    206    2887            M           2606    17448 3   user_entity_device_list fkmpoiwmgfgbh7hygtpdowdkqcb    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_entity_device_list
    ADD CONSTRAINT fkmpoiwmgfgbh7hygtpdowdkqcb FOREIGN KEY (device_list_id) REFERENCES public.device(id);
 ]   ALTER TABLE ONLY public.user_entity_device_list DROP CONSTRAINT fkmpoiwmgfgbh7hygtpdowdkqcb;
       public          postgres    false    207    202    2879            �   p   x���1� E��0�_��%<��#���+���C�����a$(PL��Ж�r�y�J�ar��幗V@+�ť�ȃ�#��2OZ7������d҉-���0�I�^�b�E�Q�      �      x�KIMK,�)�4������ '�      �   ]   x�]���0Eѵ�
�	�����06d�չ��&�+ٜS��s��І����
��������@�6V �r���]!��]Ѓ���� ^Š'n      �   h   x�5�;
1��Z:L�F~�]Ҧ�@���)<��a�7<����zCzfD�*
����6��a�����L�H�r�;t�Qh�u�b}hA����B$k2#�/��6N      �   *   x�3�41�2�41� �D�c.3NC � bK�=... ��U      �   8   x�32�L,N�4�p��gf~��aC�e�M��JA� Bs��qqq ��      �   #   x�32�44�2�`�Ds�q�s��qqq V��     